package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
)

const (
	AppTitle   = `llamacpphtmld`
	AppVersion = `0.0.0-dev` // should be overridden by go build argument
)

type Config struct {
	NetBind              string
	ModelPath            string
	SimultaneousRequests int
}

func NewConfigFromEnv() (Config, error) {
	ret := Config{
		NetBind:   os.Getenv(`LCH_NET_BIND`),
		ModelPath: os.Getenv(`LCH_MODEL_PATH`),
	}

	SimultaneousRequests, err := strconv.Atoi(os.Getenv(`LCH_SIMULTANEOUS_REQUESTS`))
	if err != nil {
		return Config{}, fmt.Errorf("LCH_SIMULTANEOUS_REQUESTS: %w", err)
	}
	ret.SimultaneousRequests = SimultaneousRequests

	if _, err := os.Stat(ret.ModelPath); err != nil {
		return Config{}, fmt.Errorf("LCH_MODEL_PATH: %w", err)
	}

	return ret, nil
}

type Application struct {
	cfg Config
	sem chan (struct{})
}

func main() {
	log.Printf("%s v%s", AppTitle, AppVersion)

	cfg, err := NewConfigFromEnv()
	if err != nil {
		log.Fatal(err)
	}

	app := Application{
		cfg: cfg,
		sem: make(chan struct{}, cfg.SimultaneousRequests), // use a buffered channel as a semaphore
	}

	router := http.NewServeMux()
	router.HandleFunc(`/`, func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set(`Server`, AppTitle+`/`+AppVersion)

		if r.Method == `GET` && r.URL.Path == `/` {
			app.GET_Root(w, r)
		} else if r.Method == `POST` && r.URL.Path == `/api/v1/generate` {
			app.POST_Chat(w, r)
		} else {
			http.Error(w, "Not found", 404)
		}
	})

	log.Printf("Listening on %s ...", cfg.NetBind)

	log.Fatal(http.ListenAndServe(cfg.NetBind, router))
}
